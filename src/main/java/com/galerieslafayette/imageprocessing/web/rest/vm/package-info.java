/**
 * View Models used by Spring MVC REST controllers.
 */
package com.galerieslafayette.imageprocessing.web.rest.vm;
