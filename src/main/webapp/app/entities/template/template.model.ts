export class Template {
    constructor(
        public id?: number,
        public name?: string,
        public code?: string,
        public description?: string,
        public width?: number,
        public height?: number,
        public margin?: number,
    ) { }
}
